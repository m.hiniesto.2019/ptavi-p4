#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import json
import socketserver
import sys
import time

PORT = int(sys.argv[1])


class SIPRequest():
    def __init__(self, data):
        self.data = data.decode('utf-8')
        self.command = None
        self.uri = None
        self.address = None
        self.result = None
        self.headers = None

    def parse(self):
        self._parse_command(self.data)
        first_nl = self.data.split('\n')[1:]
        self._parse_headers(first_nl[0])

    def _get_address(self, uri):
        address_schema = uri.split(':')
        address = address_schema[1]
        schema = address_schema[0]

        return address, schema

    def _parse_command(self, line):

       command_uri = line.split(' ')
       self.command = command_uri[0]
       self.uri = command_uri[1]
       self.address, schema = self._get_address(self.uri)
       if self.command != 'REGISTER':
           self.result = '405 Method Not Allowed'
       elif schema != 'sip':
           self.result = '416 Unsupported URI Scheme'
       else:
           self.result = '200 OK'

       #print(self.data, self.command, self.uri, self.address)

    def _parse_headers(self, first_nl):
        header = first_nl.split(':')
        headers_type = header[0]
        self.headers = header[1]



class sipRegisterHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """
    register = {}
    usuario = {}

    def process_register(self, request):
        if int(request.headers) > 0:
            self.register[self.client_address[0]] = request.headers
        if int(request.headers) == 0:
            del self.register[self.client_address[0]]

    def register2json(self, request):
        fecha = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))
        caducidad = f'{fecha} +{request.headers}'
        direccion = request.address
        if int(request.headers) > 0:
            with open('registered.json', 'r') as f:
                datos = json.load(f)
                self.usuario[direccion] = {
                    'address': self.client_address[0],
                    'expires': caducidad
                }
                datos = datos.append(self.usuario)
            with open('registered.json', 'w') as f:
                json.dump(datos, f, indent=2)
        if int(request.headers) == 0:
            with open('registered.json', 'r') as f:
                datos = json.load(f)
                del self.usuario[direccion]
                datos = datos.append(self.usuario)
            with open('registered.json', 'w') as f:
                json.dump(datos, f, indent=2)


    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]
        sip_request = SIPRequest(data)
        sip_request.parse()
        if (sip_request.command == 'REGISTER') and (sip_request.result == '200 OK'):
            self.process_register(sip_request)
            self.register2json(sip_request)
        sock.sendto(f'SIP/2.0 {sip_request.result}\r\n\r\n'.encode('utf-8'), self.client_address)

def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), sipRegisterHandler)
        print(f"Server listening in port {PORT}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)

if __name__ == "__main__":
    main()