#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

SERVER = str(sys.argv[1])
PORT = int(sys.argv[2])
ADDRESS = sys.argv[4]
EXPIRES_VALUE = int(sys.argv[5])

def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            LINE = 'REGISTER sip:' + ADDRESS + f' SIP/2.0\r\nExpires: {EXPIRES_VALUE}\r\n\r\n'
            print(LINE)
            my_socket.sendto(LINE.encode('utf-8'), (SERVER, PORT))
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    if sys.argv[3] != 'register':
        sys.exit('Usage: client_sip.py <ip> <puerto> register <sip_address> <expires_value>')
    else:
       main()